import React, { useState } from 'react';
import MovieList from './MovieList';
import Login from './login/Login';

const App: React.FC = () => {
  const [isLoggedIn, setLoggedIn] = useState<boolean>(false);

  const handleLogin = (username: string, password: string) => {
    if (username === 'toto' && password === 'titi') {
      setLoggedIn(true);
    } else {
      alert('Invalid credentials');
    }
  };

  return (
    <div className="App">
      {isLoggedIn ? (
        <MovieList />
      ) : (
        <Login onLogin={handleLogin} />
      )}
    </div>
  );
};

export default App;
