import React, { useEffect, useState } from 'react';
import axios from 'axios';
import Movie from './types';
import './MovieList.css';

const MovieList: React.FC = () => {
  const [movies, setMovies] = useState<Movie[]>([]);
  const [searchTerm, setSearchTerm] = useState<string>('');
  const [sortedMovies, setSortedMovies] = useState<Movie[]>([]);
  const [sortOrder, setSortOrder] = useState<'asc' | 'desc'>('asc');

  useEffect(() => {
    const apiUrl = 'http://localhost:3001/movies'; 

    axios.get(apiUrl)
      .then(response => {
        const data = response.data;
        setMovies(data);
        setSortedMovies(data);
      })
      .catch(error => console.error('Error fetching movies:', error));
  }, []);

  useEffect(() => {
    const filteredMovies = movies.filter(movie =>
      movie.title.toLowerCase().includes(searchTerm.toLowerCase())
    );
    setSortedMovies(filteredMovies);
  }, [searchTerm, movies]);

  const handleSortByTitle = () => {
    const newSortOrder = sortOrder === 'asc' ? 'desc' : 'asc';
    setSortOrder(newSortOrder);

    const sortedByTitle = [...sortedMovies].sort((a, b) => {
      if (newSortOrder === 'asc') {
        return a.title.localeCompare(b.title);
      } else {
        return b.title.localeCompare(a.title);
      }
    });
    setSortedMovies(sortedByTitle);
  };

  return (
    <div className="container">
      <h1>Movies</h1>
      <div className="search-and-sort">
        <input
          className="search-input"
          type="text"
          placeholder="Search by title"
          value={searchTerm}
          onChange={(e) => setSearchTerm(e.target.value)}
        />
        <button className="sort-button" onClick={handleSortByTitle}>
          Sort by Title {sortOrder === 'asc' ? '▲' : '▼'}
        </button>
      </div>
      <div className="movie-container">
        {sortedMovies.map(movie => (
          <div key={movie.id} className="movie-card">
            <h2 className="movie-title">{movie.title}</h2>
            <p className="release-date">Released on {movie.releaseDate}</p>
          </div>
        ))}
      </div>
    </div>
  );
};

export default MovieList;
