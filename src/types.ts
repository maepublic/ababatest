interface Movie {
    id: number;
    title: string;
    releaseDate: string;
  }
  
  export default Movie;
    