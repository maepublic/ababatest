# ababatest My favorite Movie List

A simple React application to display a favorite list of movies. The app allows users to search for movies, sort them by title.

## Features

- Display a list of movies with details (title, release date).
- Search for movies by title.
- Sort movies by title in ascending or descending order.

## Getting Started

### Prerequisites

Make sure you have [Node.js](https://nodejs.org/) and [npm](https://www.npmjs.com/) installed on your machine.

### Installation

1. Clone the repository:

   ```bash
   git clone https://gitlab.com/maepublic/ababatest.git

2. Navigate to the project folder:

   ```bash
   cd ababatest

3. Install dependencies:

   ```bash
   npm install

4. Start the JSON Server:

   ```bash
   json-server --watch db.json --port 3001

### Usage

Run the development server:

    ```bash
    npm start

Open http://localhost:3000 in your browser to view the app.

## Technologies Used

- React
- TypeScript
- axios (for API requests)
- JSON Server (for mock API)

## JSON Server

The app uses JSON Server to mock the API. The server is configured in `db.json`.



